<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 10; $i++)
        {
            $User = new User();
            $User->setFirstName($faker->firstName);
            $User->setName($faker->name);
            $User->setEmail($faker->email);
            $User->setDate($faker->dateTime);
            $User->setPassword($faker->password);

            $manager->persist($User);

        }
        $manager->flush();
    }
}
