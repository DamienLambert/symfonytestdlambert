<?php

declare(strict_types = 1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @return Response
     * @Route ("/")
     */
    public function index(): Response
    {
        return $this->render('index.html.twig', [
            "extensions" => get_loaded_extensions(),
        ]);
    }

    /**
     * @return Response
     * @Route ("/test")
     */
    public function accueil()
    {
        return $this->render('base.html.twig');
    }
}
