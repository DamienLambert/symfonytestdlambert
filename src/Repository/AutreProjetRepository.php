<?php

namespace App\Repository;

use App\Entity\AutreProjet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AutreProjet|null find($id, $lockMode = null, $lockVersion = null)
 * @method AutreProjet|null findOneBy(array $criteria, array $orderBy = null)
 * @method AutreProjet[]    findAll()
 * @method AutreProjet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutreProjetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AutreProjet::class);
    }

    // /**
    //  * @return AutreProjet[] Returns an array of AutreProjet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AutreProjet
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
