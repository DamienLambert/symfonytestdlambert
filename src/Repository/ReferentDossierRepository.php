<?php

namespace App\Repository;

use App\Entity\ReferentDossier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferentDossier|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferentDossier|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferentDossier[]    findAll()
 * @method ReferentDossier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferentDossierRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferentDossier::class);
    }

    // /**
    //  * @return ReferentDossier[] Returns an array of ReferentDossier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReferentDossier
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
