<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AutreProjetRepository")
 */
class AutreProjet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule_projet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntituleProjet(): ?string
    {
        return $this->intitule_projet;
    }

    public function setIntituleProjet(string $intitule_projet): self
    {
        $this->intitule_projet = $intitule_projet;

        return $this;
    }
}
