<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_entreprise;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $effectif_entreprise;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_adhesion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_stagiaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse_entreprise;

    /**
     * @ORM\Column(type="integer")
     */
    private $telephone_accueil_entreprise;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $mail_accueil_entreprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $activite_entreprise;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombre_service_entreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $action_mene_entreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $raison_sociale_entreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $partenaire_entreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $client_entreprise;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEntreprise(): ?string
    {
        return $this->nom_entreprise;
    }

    public function setNomEntreprise(string $nom_entreprise): self
    {
        $this->nom_entreprise = $nom_entreprise;

        return $this;
    }

    public function getEffectifEntreprise(): ?int
    {
        return $this->effectif_entreprise;
    }

    public function setEffectifEntreprise(?int $effectif_entreprise): self
    {
        $this->effectif_entreprise = $effectif_entreprise;

        return $this;
    }

    public function getDateAdhesion(): ?\DateTimeInterface
    {
        return $this->date_adhesion;
    }

    public function setDateAdhesion(\DateTimeInterface $date_adhesion): self
    {
        $this->date_adhesion = $date_adhesion;

        return $this;
    }

    public function getNombreStagiaire(): ?int
    {
        return $this->nombre_stagiaire;
    }

    public function setNombreStagiaire(?int $nombre_stagiaire): self
    {
        $this->nombre_stagiaire = $nombre_stagiaire;

        return $this;
    }

    public function getAdresseEntreprise(): ?string
    {
        return $this->adresse_entreprise;
    }

    public function setAdresseEntreprise(string $adresse_entreprise): self
    {
        $this->adresse_entreprise = $adresse_entreprise;

        return $this;
    }

    public function getTelephoneAccueilEntreprise(): ?int
    {
        return $this->telephone_accueil_entreprise;
    }

    public function setTelephoneAccueilEntreprise(int $telephone_accueil_entreprise): self
    {
        $this->telephone_accueil_entreprise = $telephone_accueil_entreprise;

        return $this;
    }

    public function getMailAccueilEntreprise(): ?string
    {
        return $this->mail_accueil_entreprise;
    }

    public function setMailAccueilEntreprise(string $mail_accueil_entreprise): self
    {
        $this->mail_accueil_entreprise = $mail_accueil_entreprise;

        return $this;
    }

    public function getActiviteEntreprise(): ?string
    {
        return $this->activite_entreprise;
    }

    public function setActiviteEntreprise(string $activite_entreprise): self
    {
        $this->activite_entreprise = $activite_entreprise;

        return $this;
    }

    public function getNombreServiceEntreprise(): ?int
    {
        return $this->nombre_service_entreprise;
    }

    public function setNombreServiceEntreprise(?int $nombre_service_entreprise): self
    {
        $this->nombre_service_entreprise = $nombre_service_entreprise;

        return $this;
    }

    public function getActionMeneEntreprise(): ?string
    {
        return $this->action_mene_entreprise;
    }

    public function setActionMeneEntreprise(?string $action_mene_entreprise): self
    {
        $this->action_mene_entreprise = $action_mene_entreprise;

        return $this;
    }

    public function getRaisonSocialeEntreprise(): ?string
    {
        return $this->raison_sociale_entreprise;
    }

    public function setRaisonSocialeEntreprise(?string $raison_sociale_entreprise): self
    {
        $this->raison_sociale_entreprise = $raison_sociale_entreprise;

        return $this;
    }

    public function getPartenaireEntreprise(): ?string
    {
        return $this->partenaire_entreprise;
    }

    public function setPartenaireEntreprise(?string $partenaire_entreprise): self
    {
        $this->partenaire_entreprise = $partenaire_entreprise;

        return $this;
    }

    public function getClientEntreprise(): ?string
    {
        return $this->client_entreprise;
    }

    public function setClientEntreprise(?string $client_entreprise): self
    {
        $this->client_entreprise = $client_entreprise;

        return $this;
    }
}
