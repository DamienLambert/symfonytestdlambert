<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StageRepository")
 */
class Stage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $type_stage;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_fin_stage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule_stage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $moyen_stage;

    /**
     * @ORM\Column(type="integer")
     */
    private $horaires_stage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeStage(): ?string
    {
        return $this->type_stage;
    }

    public function setTypeStage(string $type_stage): self
    {
        $this->type_stage = $type_stage;

        return $this;
    }

    public function getDateFinStage(): ?\DateTimeInterface
    {
        return $this->date_fin_stage;
    }

    public function setDateFinStage(\DateTimeInterface $date_fin_stage): self
    {
        $this->date_fin_stage = $date_fin_stage;

        return $this;
    }

    public function getIntituleStage(): ?string
    {
        return $this->intitule_stage;
    }

    public function setIntituleStage(string $intitule_stage): self
    {
        $this->intitule_stage = $intitule_stage;

        return $this;
    }

    public function getMoyenStage(): ?string
    {
        return $this->moyen_stage;
    }

    public function setMoyenStage(?string $moyen_stage): self
    {
        $this->moyen_stage = $moyen_stage;

        return $this;
    }

    public function getHorairesStage(): ?int
    {
        return $this->horaires_stage;
    }

    public function setHorairesStage(int $horaires_stage): self
    {
        $this->horaires_stage = $horaires_stage;

        return $this;
    }
}
