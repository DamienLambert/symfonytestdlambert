<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EvenementRepository")
 */
class Evenement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $periode_event;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description_event;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeriodeEvent(): ?string
    {
        return $this->periode_event;
    }

    public function setPeriodeEvent(?string $periode_event): self
    {
        $this->periode_event = $periode_event;

        return $this;
    }

    public function getDescriptionEvent(): ?string
    {
        return $this->description_event;
    }

    public function setDescriptionEvent(?string $description_event): self
    {
        $this->description_event = $description_event;

        return $this;
    }
}
