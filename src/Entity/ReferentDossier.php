<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferentDossierRepository")
 */
class ReferentDossier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_referent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom_referent;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $status_referent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numero_fixe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numero_portable;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $email_referent;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptif;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomReferent(): ?string
    {
        return $this->nom_referent;
    }

    public function setNomReferent(string $nom_referent): self
    {
        $this->nom_referent = $nom_referent;

        return $this;
    }

    public function getPrenomReferent(): ?string
    {
        return $this->prenom_referent;
    }

    public function setPrenomReferent(string $prenom_referent): self
    {
        $this->prenom_referent = $prenom_referent;

        return $this;
    }

    public function getStatusReferent(): ?string
    {
        return $this->status_referent;
    }

    public function setStatusReferent(string $status_referent): self
    {
        $this->status_referent = $status_referent;

        return $this;
    }

    public function getNumeroFixe(): ?int
    {
        return $this->numero_fixe;
    }

    public function setNumeroFixe(?int $numero_fixe): self
    {
        $this->numero_fixe = $numero_fixe;

        return $this;
    }

    public function getNumeroPortable(): ?int
    {
        return $this->numero_portable;
    }

    public function setNumeroPortable(?int $numero_portable): self
    {
        $this->numero_portable = $numero_portable;

        return $this;
    }

    public function getEmailReferent(): ?string
    {
        return $this->email_referent;
    }

    public function setEmailReferent(string $email_referent): self
    {
        $this->email_referent = $email_referent;

        return $this;
    }

    public function getDescriptif(): ?string
    {
        return $this->descriptif;
    }

    public function setDescriptif(?string $descriptif): self
    {
        $this->descriptif = $descriptif;

        return $this;
    }
}
