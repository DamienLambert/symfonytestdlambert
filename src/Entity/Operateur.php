<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OperateurRepository")
 */
class Operateur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_operateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomOperateur(): ?string
    {
        return $this->nom_operateur;
    }

    public function setNomOperateur(string $nom_operateur): self
    {
        $this->nom_operateur = $nom_operateur;

        return $this;
    }
}
