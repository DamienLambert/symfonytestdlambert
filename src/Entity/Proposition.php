<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PropositionRepository")
 */
class Proposition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule_proposition;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type_proposition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntituleProposition(): ?string
    {
        return $this->intitule_proposition;
    }

    public function setIntituleProposition(string $intitule_proposition): self
    {
        $this->intitule_proposition = $intitule_proposition;

        return $this;
    }

    public function getTypeProposition(): ?string
    {
        return $this->type_proposition;
    }

    public function setTypeProposition(string $type_proposition): self
    {
        $this->type_proposition = $type_proposition;

        return $this;
    }
}
