require('../css/app.css');

import Vue from 'vue';

import PhpExtensions from './components/PhpExtensions';

new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    components: {PhpExtensions}
});